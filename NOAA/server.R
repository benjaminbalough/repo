

##-----------------
## NOAA Server File
##-----------------

options(shiny.trace=TRUE) 
options(shiny.sanitize.errors = TRUE)

if (interactive()){
shinyServer(function(input, output, session){ 
  
  ##---------
  ## On Launch
  ##----------
  
  newuser <- Sys.getenv("USER")
  
  sendSweetAlert(
    session = session,
    title = paste0("Welcome, ", newuser),
    text = span("OST NIM Demo"),
    html = TRUE,
    type = "success"
  )
  
  observe({
    req(input$mypick)
    
    nodes <- nodes10 %>% filter(Name %in% input$mypick)
    links <- links10 %>% filter(from %in% input$mypick | to %in% input$mypick)

    ##------------------------------
    ## Model 1: Node Size by Budget
    ##------------------------------
    
    nodes2 <- data.frame(id = nodes$Name,
                         group = nodes$Type,
                         size = as.numeric(nodes$Funding),
                         Type = nodes$Type)
    edges2 <- data.frame(from = links$from, to = links$to,
                         width = links$weight)
    
    
    NIM_1 <- visNetwork(nodes = nodes2,
                        edges = edges2,
                        main = "Budget",
                        submain = "Node Color by Type, Node Size by Funding, Edge Width by Weight",
                        footer = "") %>% 
      visNodes(shape = "circle", shadow = F, size = "nodes2$size", color = "nodes2$Type") %>%
      visEdges(width = "width") %>%
      visOptions(selectedBy = "Type",
                 highlightNearest = list(enabled = TRUE,
                                         degree = 2,
                                         hover = TRUE,
                                         labelOnly = TRUE),
                 nodesIdSelection = list(enabled = TRUE,
                                         values = nodes2$id)) %>% 
      visInteraction(navigationButtons = TRUE) %>%
      visPhysics(repulsion = list(springlength = 30),
                 maxVelocity = 0,
                 solver = "forceAtlas2Based",
                 forceAtlas2Based = list(gravitationalConstant = -300),
                 timestep = 0.25)
    
    output$vis1 <- renderVisNetwork({NIM_1})
    
    ##--------------------------------------
    ## Model 2: Size by Contractor Workforce
    ##--------------------------------------
    
    ##  Node Color by Type, Node Size by Workforce, Edge Width by CriticalityxUniqueness
    
    links$CU_Multiplier <- links$`Mission Critical` * links$Uniqueness
    
    nodes3 <- data.frame(id = nodes$Name,
                         size = as.numeric(nodes$Workforce),
                         group = nodes$Type,
                         Type = nodes$Type)
    edges3 <- data.frame(from = links$from, to = links$to,
                         width = links$Uniqueness)
    
    NIM_2 <- visNetwork(nodes = nodes3,
                        edges = edges3,
                        main = "Workforce",
                        submain = "Node Color by Type, Node Size by Workforce, Edge Width by Uniqueness",
                        footer = "") %>% 
      visNodes(shape = "circle", shadow = F, size = "nodes3$size", color = "nodes3$Type") %>%
      visEdges(width = "width") %>%
      visOptions(selectedBy = "Type",
                 highlightNearest = list(enabled = TRUE,
                                         degree = 2,
                                         hover = TRUE,
                                         labelOnly = TRUE),
                 nodesIdSelection = list(enabled = TRUE,
                                         values = nodes3$id)) %>% 
      visInteraction(navigationButtons = TRUE) %>%
      visPhysics(repulsion = list(springlength = 30),
                 maxVelocity = 0,
                 solver = "forceAtlas2Based",
                 forceAtlas2Based = list(gravitationalConstant = -300),
                 timestep = 0.25)
    
    # 
    
    output$vis2 <- renderVisNetwork({NIM_2})
    
    ##-----------------------------------
    ## Model 3: Looking at New Attributes
    ##-----------------------------------
    
    ### Model 3: Node Color by Type, Node Size by Sum Weight, Edge Width by Edge Betweeness
    
    for (i in 1:nrow(nodes)){
      slice               <- links %>% filter(from == nodes$Name[i] | to == nodes$Name[i])
      nodes$Sum_Weight[i] <- sum(slice$weight)  
    }

    nodes4 <- data.frame(id = nodes$Name,
                         size = as.numeric(nodes$Sum_Weight),
                         group = nodes$Type,
                         Type = nodes$Type)
    edges4 <- data.frame(from = links$from, to = links$to,
                         width = links$e_btwn *.3)
    
    NIM_3 <- visNetwork(nodes = nodes4,
                        edges = edges4,
                        main = "Importance",
                        submain = "Color by Type, Size by Sum(Weight), Width by Edge Betweenness",
                        footer = "") %>% 
      visNodes(shape = "circle", shadow = F, size = "nodes4$size", color = "nodes4$Type") %>%
      visEdges(width = "width") %>%
      visOptions(selectedBy = "Type",
                 highlightNearest = list(enabled = TRUE,
                                         degree = 2,
                                         hover = TRUE,
                                         labelOnly = TRUE),
                 nodesIdSelection = list(enabled = TRUE,
                                         values = nodes4$id)) %>% 
      visInteraction(navigationButtons = TRUE) %>%
      visPhysics(repulsion = list(springlength = 30),
                 maxVelocity = 0,
                 solver = "forceAtlas2Based",
                 forceAtlas2Based = list(gravitationalConstant = -300),
                 timestep = 0.25)

    
    output$vis3 <- renderVisNetwork({NIM_3})
    
    ##-----------------
    ## Metadata Section
    ##-----------------
    
    output$zoom_funding    <- renderInfoBox({
      infoBox(
        title = "Total Funding", 
        subtitle = "(USD)",
        value = sum(as.numeric(nodes$Funding), na.rm = TRUE), 
        icon = icon("usd", lib = "glyphicon"),
        color = "aqua", fill = TRUE
      )
    })
    output$zoom_fte        <- renderInfoBox({
      infoBox(
        title = "Total FTEs", 
        subtitle = "Staff",
        value = sum(nodes$`FTE Staff`, na.rm = TRUE), 
        icon = icon("user", lib = "glyphicon"),
        color = "fuchsia", fill = TRUE
      )
    })
    output$zoom_me         <- renderInfoBox({
      infoBox(
        title = "Total Contractors", 
        subtitle = "Workforce",
        value = sum(nodes$Workforce, na.rm = TRUE), 
        icon = icon("user", lib = "glyphicon"),
        color = "purple", fill = TRUE
      )
    })
    output$zoom_between    <- renderInfoBox({
      infoBox(
        title = "Avg Betweenness", 
        subtitle = "By Name",
        value = round(mean(nodes$n_btwn), digits = 2),
        icon = icon("resize-horizontal", lib = "glyphicon"),
        color = "green", fill = TRUE
      )
    })
    output$zoom_hub        <- renderInfoBox({
      infoBox(
        title = "Avg Hubs", 
        subtitle = "By Name",
        value = round(mean(nodes$hubs), digits = 2),
        icon = icon("screenshot", lib = "glyphicon"),
        color = "orange", fill = TRUE
      )
    })
    output$zoom_centrality <- renderInfoBox({
      infoBox(
        title = "Avg Centrality", 
        subtitle = "By Name",
        value = round(mean(nodes$centrality), digits = 2),
        icon = icon("transfer", lib = "glyphicon"),
        color = "blue", fill = TRUE
      )
    })
    
    ##-----------------
    ## Home Page Graphs
    ##-----------------
    
    output$h3   <- renderPlotly({
      
      nodes <- na.omit(nodes)
      colnames(nodes)[13] <- "Betweenness"
      nods <- nodes %>% select(Name, Betweenness, hubs, centrality, Funding, `FTE Staff`, Workforce)
      
      nods   <- data.frame(nods) %>% arrange(Funding, Name)
      nods$Name <- factor(nods$Name, levels = c(as.character(nods$Name)))
      
      p <- plot_ly(nods, y = ~Name, x = ~centrality, mode = "lines+markers", type = "scatter",
                   name = "Centrality", line = list(color = "blue"), marker = list(color = "blue")) %>%
        add_trace(x = ~hubs, name = "Hubs", line = list(dash = "dot", color = "orange"), marker = list(color = "orange")) %>%
        add_trace(x = ~Betweenness, name = "Betweenness", line = list(dash = "dash", color = "green"), marker = list(color = "green")) %>% 
        layout(title = "",
               yaxis = list(title = ""),
               xaxis = list(title = "",rangemode = "tozero"));p
      
      p2 <- plot_ly(nods, y = ~Name, x = ~FTE.Staff, mode = "lines+markers", type = "scatter",
                    name = "FTE Staff", line = list(color = "fuchsia", width = 5, dash = "dash"), marker = list(color = "fuchsia")) %>%
        add_trace(x = ~Workforce, name = "Contractor Workforce", line = list(dash = "dot", color = "purple",
                                                                                        width = 5),
                  marker = list(symbol = "square", color = "purple")) %>% 
        layout(title = "",
               yaxis = list(title = "", rangemode = "tozero"),
               xaxis = list(title = ""));p2
      
      
      p3 <- plot_ly(nods, y = ~Name, x = ~Funding, mode = "lines+markers", type = "scatter",
                    name = "Funding", line = list(color = "aqua", width = 5), marker = list(color = "aqua")) %>%
      #  add_text(x = ~Name, y = ~Funding - 10000, text = ~paste("$", Funding)) %>%
        layout(title = "",
               yaxis = list(title = "", rangemode = "tozero"),
               xaxis = list(title = ""))
      
      p4 <- subplot(p2, p, p3, nrows = 1, shareY = T) %>%
        layout(xaxis = list(tickfont = list(size = 12), tickangle = 15)) %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'));p4
      
      
      return({p4})
    })
    output$ind1 <- renderPlotly({

      nods      <- data.frame(nodes10) %>% arrange(centrality, Name)
      nods      <- na.omit(nods)
      nods$Name <- factor(nods$Name, levels = c(as.character(nods$Name)))
      
      for (i in 1:nrow(nods)){
        if (nods$Name[i] %in% input$mypick){nods$Col[i] <- "Yes"}
        else {nods$Col[i] <- "No"}
      }
      
      p <- plot_ly(nods, y = ~Name, x = ~centrality,
                   name = "Centrality", color = ~Col, 
                   type = 'bar', orientation = 'h', colors = c("lightgrey", "navy")) %>%
        layout(title = "Centrality", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'));p
      return({p})
      
    })
    output$ind2 <- renderPlotly({
      nods2      <- data.frame(nodes10) %>% arrange(n_btwn, Name)
      nods2      <- na.omit(nods2)
      nods2$Name <- factor(nods2$Name, levels = c(as.character(nods2$Name)))
      
      for (i in 1:nrow(nods2)){
        if (nods2$Name[i] %in% input$mypick){nods2$Col[i] <- "Yes"}
        else {nods2$Col[i] <- "No"}
      }
      
    
      p2 <- plot_ly(nods2, y = ~Name, x = ~n_btwn, name = "Betweenness", color = ~Col, 
                    type = 'bar', orientation = 'h',
                    colors = c("lightgrey", "darkgreen"))  %>%
        layout(title = "Betweenness", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'))
      return(p2)
      
    })
    output$ind3 <- renderPlotly({
      nods3   <- data.frame(nodes10) %>% arrange(hubs, Name)
      nods3 <- na.omit(nods3)
      nods3$Name <- factor(nods3$Name, levels = c(as.character(nods3$Name)))
      
      for (i in 1:nrow(nods3)){
        if (nods3$Name[i] %in% input$mypick){nods3$Col[i] <- "Yes"}
        else {nods3$Col[i] <- "No"}
      }
      
      
      p3 <- plot_ly(nods3, y = ~Name, x = ~hubs, name = "Hubs", color = ~Col, 
                    type = 'bar', orientation = 'h',
                    colors = c("lightgrey", "orange"))  %>%
        layout(title = "Hubs", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'))
      
      return({p3})  
      
      
    })
    output$ind4 <- renderPlotly({
      
      nods      <- data.frame(nodes10) %>% arrange(FTE.Staff, Name)
      nods      <- na.omit(nods)
      nods$Name <- factor(nods$Name, levels = c(as.character(nods$Name)))
      
      for (i in 1:nrow(nods)){
        if (nods$Name[i] %in% input$mypick){nods$Col[i] <- "Yes"}
        else {nods$Col[i] <- "No"}
      }
      
      p <- plot_ly(nods, y = ~Name, x = ~FTE.Staff,
                   name = "FTE Staff", color = ~Col, 
                   type = 'bar', orientation = 'h', colors = c("lightgrey", "pink")) %>%
        layout(title = "FTE Staff", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'));p
      return({p})
      
    })
    output$ind5 <- renderPlotly({
      
      nods      <- data.frame(nodes10) %>% arrange(Workforce, Name)
      nods      <- na.omit(nods)
      nods$Name <- factor(nods$Name, levels = c(as.character(nods$Name)))
      
      for (i in 1:nrow(nods)){
        if (nods$Name[i] %in% input$mypick){nods$Col[i] <- "Yes"}
        else {nods$Col[i] <- "No"}
      }
      
      p <- plot_ly(nods, y = ~Name, x = ~Workforce,
                   name = "Contractor Workforce", color = ~Col, 
                   type = 'bar', orientation = 'h', colors = c("lightgrey", "purple")) %>%
        layout(title = "Contractor Workforce", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'));p
      return({p})
      
    })
    output$ind6 <- renderPlotly({
      
      nods      <- data.frame(nodes10) %>% arrange(Funding, Name)
      nods      <- na.omit(nods)
      nods$Name <- factor(nods$Name, levels = c(as.character(nods$Name)))
      
      for (i in 1:nrow(nods)){
        if (nods$Name[i] %in% input$mypick){nods$Col[i] <- "Yes"}
        else {nods$Col[i] <- "No"}
      }
      
      p <- plot_ly(nods, y = ~Name, x = ~Funding,
                   name = "Funding", color = ~Col, 
                   type = 'bar', orientation = 'h', colors = c("lightgrey", "lightblue")) %>%
        layout(title = "Funding", showlegend = F)  %>%
        layout(legend = list(x = 0.1, y = 1, orientation = 'h'));p
      return({p})
      
    })
  })
  

  ##---------------
  ## Raw Datatables
  ##---------------
  
  output$nodetab <- DT::renderDT({
    out2 <- datatable(
      nodes, rownames = FALSE, filter = 'bottom', extensions = c('Buttons', 'Responsive'), options = list(
        dom = 'Bfrtip',
        deferRender = TRUE,
        scrollY = 200,
        paging = FALSE,
        scroller = TRUE,
        buttons = 
          list(list(
            extend = 'collection',
            buttons = list(list(extend='csv',
                                filename = 'NIM_nodes_data'),
                           list(extend='excel',
                                filename = 'NIM_nodes_data')),
            text = 'Download')),
        scrollX = TRUE,
        fixedColumns = TRUE,
        initComplete = JS(
          "function(settings, json) {",
          "$(this.api().table().header()).css({'background-color': '#000', 'color': '#fff'});",
          "}")
      )
    )
    return(out2)
  })
  output$linktab <- DT::renderDT({
    out2 <- datatable(
      links10, rownames = FALSE, filter = 'bottom', extensions = c('Buttons', 'Responsive'), options = list(
        dom = 'Bfrtip',
        deferRender = TRUE,
        scrollY = 200,
        paging = FALSE,
        scroller = TRUE,
        buttons = 
          list(list(
            extend = 'collection',
            buttons = list(list(extend='csv',
                                filename = 'NIM_nodes_data'),
                           list(extend='excel',
                                filename = 'NIM_nodes_data')),
            text = 'Download')),
        scrollX = TRUE,
        fixedColumns = TRUE,
        initComplete = JS(
          "function(settings, json) {",
          "$(this.api().table().header()).css({'background-color': '#000', 'color': '#fff'});",
          "}")
      )
    )
    return(out2)
  })
  
  
})} ## End of shinyServer  


##-------------------------
## Model 4: Not Really Sure
##-------------------------

#Model 4: Node Color by Type, Node Size by Hub, Edge Width by Edge Betweeness
# nodes5 <- data.frame(id = nodes$Name,
#                      size = as.numeric(nodes$hubs),
#                      group = nodes$Type,
#                      Type = nodes$Type)
# edges5 <- data.frame(from = links$from, to = links$to,
#                      width = links$e_btwn *.3)
# 
# NIM_4 <- visNetwork(nodes = nodes5,
#                     edges = edges5,
#                     main = "Model IV",
#                     submain = "Color by Type, Size by Hub, Width by Edge Betweenness",
#                     footer = "") %>% 
#   visNodes(shape = "circle", shadow = F, size = "nodes5$size", color = "nodes5$Type") %>%
#   visEdges(arrows = "to", color = "grey", width = "width") %>%
#   visOptions(selectedBy = "Type",
#              highlightNearest = list(enabled = TRUE,
#                                      degree = 2,
#                                      hover = TRUE,
#                                      labelOnly = TRUE),
#              nodesIdSelection = list(enabled = TRUE,
#                                      values = nodes5$id)) %>% 
#   visInteraction(navigationButtons = TRUE) %>% 
#   visPhysics(repulsion = list(springlength = 30), 
#              maxVelocity = 0,
#              solver = "forceAtlas2Based",
#              forceAtlas2Based = list(gravitationalConstant = -1000),
#              timestep = 0.25)
# 
# output$vis4 <- renderVisNetwork({NIM_4})

