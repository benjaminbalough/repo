##------------
## LUCA Global
##------------

#source("https://install-github.me/mangothecat/shinytoastr")

## Libraries 
library(RODBC); library(dplyr); library(leaflet); library(shinybusy)
library(DBI); library(odbc); library(shinydashboard); library(shinydashboardPlus)
library(shinycssloaders); library(shinyWidgets); library(shiny.i18n)
library(rintrojs); library(pdftools); library(shinytoastr)
library(cld2); library(pushoverr)


countries <- c("English", "French", "Portugeuse", "Spanish")

flags <- c(
  "https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/gb.svg",
  "https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/fr.svg",
  "https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/br.svg",
  "https://cdn.rawgit.com/lipis/flag-icon-css/master/flags/4x3/es.svg"
)

pushoverr::set_pushover_user(user = "ugbdnvsiiuzvdpp64w4t7czea5rc2p")
pushoverr::set_pushover_app(token = "a954zdkk3tdjwszqrjnw9ge8qsrmar")

#pushover(message = "Cognitive Audit Assistant: Your Testing is Complete")
